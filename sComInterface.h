/*
 * .h
 */
#ifndef sComInterface_h
#define sComInterface_h

#include "Arduino.h"

const byte NumOfVariables = 4;
enum iVARS {intA, intB, intC, intD};

enum fVARS {floatA, floatB, floatC, floatD};

class sComInterface 
{
  public:
    sComInterface() {holdProgram = false;}
    ~sComInterface() {}

    // Methods
    void begin(long = 9600);
    void checkBuffer(bool holdWhenCome = false);

    // methods to set and get values vithin the source code
    void setVal(iVARS i, long value) {iVals[i] = value;}
    void setVal(fVARS f, float value) {fVals[f] = value;}
    long getVal(iVARS i) {return iVals[i];}
    float getVal(fVARS f) {return fVals[f];}

  private:
    // internal values for programs
    long iVals[NumOfVariables];
    float fVals[NumOfVariables];

    // serial data
    const char *iNames[NumOfVariables] = {"intA", "intB", "intC", "intD"};
    const char *fNames[NumOfVariables] = {"floatA", "floatB", "floatC", "floatD"};
    bool holdProgram;
    
    String serialRead();
    void requestValueCmd(String);
    int findIntVar(String);
    int findFloatVar(String);
    void errorMsg();
    void ioCmd(String);
    bool validInt(String);
    bool validFloat(String&);
    void assignValueCmd(String);
    void singleCmd(String);
};

#endif
