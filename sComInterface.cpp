/*
 * .cpp file
*/

#include "Arduino.h"
#include "sComInterface.h"

void sComInterface::begin(long baudRate) {
  Serial.begin(baudRate);
}

void sComInterface::checkBuffer(bool holdWhenCome) {
  holdProgram = holdWhenCome;
  do {
  if (Serial.available()) {
    
    // serial data input + msg to serial monitor
    String serialData = serialRead();
    Serial.println((String) "IN:\t" + serialData);

    Serial.print("OUT:\t");
    requestValueCmd(serialData);
    ioCmd(serialData);
    assignValueCmd(serialData);
    singleCmd(serialData);
    } 
  } while(holdProgram);
}

String sComInterface::serialRead() {
  char dataByte;
  String inputData = "";
  while(Serial.available()) {
      dataByte = char(Serial.read());
      if(dataByte == ' ') // All spaces are removed
        continue;
      inputData += (char)dataByte;
      delay(3); // intentional delay for buffer to load
  }
  return inputData;
}

// function processes cmds ends with "?"
void sComInterface::requestValueCmd(String inputData) {
  // request commands
  int dataIdx = inputData.indexOf('?');
  if (dataIdx != -1) {
    String prefixStr = inputData.substring(0,dataIdx);
    if (prefixStr.length() + 1 != inputData.length())
      errorMsg();
    else {
      int intIdx = findIntVar(prefixStr);
      int floatIdx = findFloatVar(prefixStr);
      if (intIdx != -1)
        Serial.println((String)iNames[intIdx] + " = " + iVals[intIdx]);
      else if (floatIdx != -1)
        Serial.println((String)fNames[floatIdx] + " = " + fVals[floatIdx]);
      else
        errorMsg();
    }
  }
}

// general error message
void sComInterface::errorMsg() {
  Serial.println("Invalid command");
}

// function returns index of requested long value otherwise returns -1
int sComInterface::findIntVar(String data) {
  for (int i = 0; i < NumOfVariables; i++) // search for variable
    if (data == iNames[i])
      return i;

  return -1;
}

// function returns index of requested float value otherwise returns -1
int sComInterface::findFloatVar(String data) {
  for (int i = 0; i < NumOfVariables; i++) // search for variable
    if (data == fNames[i])
      return i;

  return -1;
}

// function processes input/output cmds. They contain "."
void sComInterface::ioCmd(String inputData) {

  int dotIdx = inputData.indexOf('.');
  if (dotIdx != -1) {
    String prefixStr = inputData.substring(0,dotIdx);
    String postfixStr = inputData.substring(dotIdx + 1);
    if (prefixStr == "read") {
      int value;
      if      (postfixStr == "I1") value = analogRead(A6);
      else if (postfixStr == "I2") value = analogRead(A7);
      else{
        errorMsg();
        goto here;
      }
      Serial.println((String) "Voltage at " + postfixStr + " = " + value);
      here: ;   
    }
    else if (prefixStr == "write") {
      int equalIdx = postfixStr.indexOf('=');
      if(equalIdx != -1){
        String pin = postfixStr.substring(0,equalIdx);
        String outVal = postfixStr.substring(equalIdx + 1);
        
        int pinNumber;
        if (pin == "O1")      pinNumber = 9;
        else if (pin == "O2") pinNumber = 10;
        else                  pinNumber = -1;
  
        if (pinNumber != -1 && outVal.length() > 0) {
          if (outVal == "ON")         analogWrite(pinNumber, 255);
          else if (outVal == "OFF")  analogWrite(pinNumber, 0);          
          else if (validInt(outVal)) {
            int intNum = outVal.toInt();
            if (intNum < 0 || intNum > 255) goto err/*Serial.println("ERROR: Values must be within range 0 - 255")*/;
            else analogWrite(pinNumber, intNum);
          }
          Serial.println((String) "Voltage at " + pin + " = " + outVal);
        }
        else errorMsg();  
      }
      else errorMsg();   
    }
    else err: errorMsg();
  }
}

// function returns true if string is valid number otherwise returns false
bool sComInterface::validInt(String numStr) {
  byte len = numStr.length();
  if (len == 0) return false;
  for (int i = 0; i < len; i++){          
    if (!(isDigit(numStr.charAt(i)))){ // all characters must be digits
      if (numStr.charAt(0) == '-' && len > 1) //first character can be minus sign as well
        continue;
      return false;
    }
  }
  return true;
}

// function returns true if string is valid number otherwise returns false
bool sComInterface::validFloat(String& numStr) {
  bool decimalPoint = false;
  byte len = numStr.length();
  if (len == 0) return false;
  numStr.replace(",", "."); // replacing commas for dec. points
  for (int i = 0; i < len;i++){          
    if (!(isDigit(numStr.charAt(i)))){
      if (numStr.charAt(0) == '-' && len > 1) //first sign can be minus sign as well
        continue;
      else if (numStr.charAt(i) == '.' && decimalPoint == false){ // number can contain only 1 dec. point
        decimalPoint = true;
        continue;
      }        
      return false;
    }
  }
  return true;
}

// function processes float and int vals assigning cmds. They contain "="
void sComInterface::assignValueCmd(String inputData) {
  int equalIdx = inputData.indexOf('=');
  int dotIdx = inputData.indexOf('.');
  if (equalIdx != -1 && dotIdx == -1) {
    String prefixStr = inputData.substring(0,equalIdx);
    String postfixStr = inputData.substring(equalIdx + 1);

    int intIdx = findIntVar(prefixStr);
    int floatIdx = findFloatVar(prefixStr);
    if (intIdx != -1){
      if(validInt(postfixStr)) {
        iVals[intIdx] = postfixStr.toInt();
        Serial.println((String)iNames[intIdx] + " = " + iVals[intIdx]);
      }
      else
        errorMsg();
    }
    else if (floatIdx != -1) {
      if(validFloat(postfixStr)) {
        fVals[floatIdx] = postfixStr.toFloat();
        Serial.println((String)fNames[floatIdx] + " = " + fVals[floatIdx]);
      }
      else
        errorMsg();
    }
    else
      errorMsg();
  }
}

// function processes singleword cmds wihout operator
void sComInterface::singleCmd(String inputData) {
  if (inputData.length() > 0 && inputData.indexOf('=') == -1 && inputData.indexOf('.') == -1 && inputData.indexOf('?') == -1){
    if(inputData == "help")
      Serial.println("Search in README.txt");
    else if (inputData == "hold"){
      Serial.println("Processes frozen");
      holdProgram = true;
    }
    else if (inputData == "run") {
      Serial.println((String)"Processes in progress");
      holdProgram = false;
    }
    else if (inputData == "status") {
      Serial.print("O1 voltage: ");
      if(!bitRead(TCCR1A,7))
        Serial.println(255*digitalRead(9));
      else
        Serial.println(OCR1A);
      Serial.print("\tO2 voltage: ");
      if(!bitRead(TCCR1A,5))
        Serial.println(255*digitalRead(10));
      else
        Serial.println(OCR1B);
      Serial.print("\tI1 voltage: ");
      Serial.println(analogRead(A6));
      Serial.print("\tI2 voltage: ");
      Serial.println(analogRead(A7));
    }
    else
      errorMsg();
  }
}
