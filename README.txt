sComInterface

sComInterface is a library for real time control of some periferals (pins D5, D6 as analog output and A6, A7 for analog input)
in your microcontroller (Uno, Nano) through serial port using Arduino IDE in your computer.

What you can/need to do in a source code:

1.initialize instance of library
sComInteraface name_of_the_instance;

2.initialize comunication using command
name_of_the_instance.begin(baud_rate); // baud_rate is default on 9600 bps

3.everytime you want to check input buffer type:
name_of_the_instance.checkBuffer(false); // if true, program will stop there and wait to your command "run"

4.you can manipulate 4 int32_t and 4 float numbers (intA-intD, floatA-floatD) using commands:
name_of_the_instance.setVal(intC, -3210);
name_of_the_instance.getVal(intC); // returns -3210

What you can do with your pc:

1.set or ask for a every of internal variables (int and float):
floatA?
floatA=-326.28

2.ask for a value at analog inputs (A6-I1, A7-I2) using read command:
read.I1

3.set value (pwm duty cycle) at analog outputs(D5-O1, D6-O2) using command:
write.O1=123 // values between 0-255 (you can use also ON for 255 and OFF for 0 write.O1=ON

4.you can manually hold code execution at nearest checkBuffer() using command:
hold // restart execution using command run

5.you can ask for all input/output information at once using command:
status

5.command "help" will send you here